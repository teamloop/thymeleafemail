def ctx = [
	displayName: 'Retailer McRetailer',
	fromEmail: 'CustomerCentral@southwire.com',
	secureResetPasswordUrl: 'http://test.com',
	fromDisplayName : 'Southwire Customer Central',
	email: 'retail@garvin.com',
	secureBaseUrl: 'https://southwiredirect.local:9002/direct',
	mediaSecureBaseUrl: 'https://southwiredirect.local:9002/direct',
	contactUs: '847-455-0188',
	contactEmail: 'Garvin.Info@southwire.com',
	secureLoginUrl: 'http://test.com',
]

thymeleafTemplateTester.generate('PendingCustomerRegistrationEmailTemplate', 'southwireDirectContentCatalog', 'Online', ctx);
