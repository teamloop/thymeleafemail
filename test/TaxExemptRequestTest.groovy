def ctx = [
	name: 'Retailer McRetailer',
	fromEmail: 'CustomerCentral@southwire.com',
	fromDisplayName : 'Southwire Customer Central',
	email: 'retail@garvin.com',
	secureBaseUrl: 'https://southwiredirect.local:9002/direct',
	mediaSecureBaseUrl: 'https://southwiredirect.local:9002/direct',
	customerEmail: 'test@test.com',
	contactPhone: '5555554444',
	taxId: 'a1b2c3d4',
]

thymeleafTemplateTester.generate('TaxExemptRequestEmailTemplate', 'southwireDirectContentCatalog', 'Online', ctx);
