thymeleafTemplateTester.generate('ShippingNotificationEmailTemplate', 'southwireDirectContentCatalog', 'Online', [
	displayName: 'Retailer McRetailer',
	fromEmail: 'CustomerCentral@southwire.com',
	fromDisplayName : 'Southwire Customer Central',
	email: 'retail@garvin.com',
	secureBaseUrl: 'https://southwiredirect.local:9002/direct',
	mediaSecureBaseUrl: 'https://southwiredirect.local:9002/direct',
	shippingNotification: [
		sapOrderNumber: '0009012001',
		customerPONumber: '41165-84',
		shipDate: '6/12/2021',
		deliveryNumber: '0084319010',
		proNumber: '4674475974',
		shipFromPlantName: 'Southwire Energy CSC',
		shipToName: 'OTTER TAIL POWER COMPANY',
		shipToAddress: '26601 ALISO CREEK RD',
		city: 'ALISO VIEJO',
		region: 'California',
		postalCode: '92656',
	],
]);
