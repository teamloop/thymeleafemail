thymeleafTemplateTester.generate('OrderConfirmationEmailTemplate', 'southwireDirectContentCatalog', 'Online', [
	displayName: 'Retailer McRetailer',
	fromEmail: 'CustomerCentral@southwire.com',
	fromDisplayName : 'Southwire Customer Central',
	email: 'retail@garvin.com',
	secureBaseUrl: 'https://southwiredirect.local:9002/direct',
	mediaSecureBaseUrl: 'https://southwiredirect.local:9002/direct',
	secureMyAccountUrl: 'https://test.com',
	order: [
		sapOrderNumber: '11112223',
		deliveryAddress: [
			firstName: 'Retailer',
			lastName: 'McRetailer',
			line1: '26601 ALISO CREEK RD',
			line2: 'Ste 300',
			town: 'ALISO VIEJO',
			region: [
				name: 'California',
			],
			postalCode: '92656',
		],
		totalPriceWithTax: [
			formattedValue: '$22.36',
		],
		orderDiscounts: [
			value: 3.00,
			formattedValue: '$3.00',
		],
		deliveryCost: [
			value: 5.00,
			formattedValue: '$5.00',
		],
		totalTax: [
			value: 2.00,
			formattedValue: '$2.00',
		],
		orderPlaceDate: '23/06/2021',
		orderPlaceTime: '03:57:55',
		orderComments: 'Some comments for order, Some comments for order, Some comments for order, Some comments for order',
		deliveryInstructions: 'Some comments for delivery, Some comments for delivery, Some comments for delivery, Some comments for delivery',
		deliveryMode: [
			name: 'UPS Ground',
		],
		entries: [
			[
				quantity: 1,
				product: [
					name: 'One In. Rigid Conduit Locknut, 50 Pak',
				],
				totalPrice: [
					formattedValue: '$0.42',
				]
			],
			[
				quantity: 25,
				product: [
					name: '1-1/2 in. Rigid Conduit Locknut',
				],
				totalPrice: [
					formattedValue: '$21.25',
				]
			],
		],
	],
]);

thymeleafTemplateTester.generate('PickupOrderConfirmationEmailTemplate', 'southwireDirectContentCatalog', 'Online', [
	displayName: 'Retailer McRetailer',
	fromEmail: 'CustomerCentral@southwire.com',
	fromDisplayName : 'Southwire Customer Central',
	email: 'retail@garvin.com',
	secureBaseUrl: 'https://southwiredirect.local:9002/direct',
	mediaSecureBaseUrl: 'https://southwiredirect.local:9002/direct',
	secureMyAccountUrl: 'https://test.com',
	pickupAddress: [
		firstName: 'Retailer',
		lastName: 'McRetailer',
		line1: '26601 ALISO CREEK RD',
		line2: 'Ste 300',
		town: 'ALISO VIEJO',
		region: [
			name: 'California',
		],
		postalCode: '92656',
	],
	order: [
		sapOrderNumber: '11112223',
		deliveryAddress: [
			firstName: 'Retailer',
			lastName: 'McRetailer',
			line1: '26601 ALISO CREEK RD',
			line2: 'Ste 300',
			town: 'ALISO VIEJO',
			region: [
				name: 'California',
			],
			postalCode: '92656',
		],
		totalPriceWithTax: [
			formattedValue: '$22.36',
		],
		orderDiscounts: [
			value: 3.00,
			formattedValue: '$3.00',
		],
		deliveryCost: [
			value: 5.00,
			formattedValue: '$5.00',
		],
		totalTax: [
			value: 2.00,
			formattedValue: '$2.00',
		],
		orderPlaceDate: '23/06/2021',
		orderPlaceTime: '03:57:55',
		orderComments: 'Some comments for order, Some comments for order, Some comments for order, Some comments for order',
		deliveryInstructions: 'Some comments for delivery, Some comments for delivery, Some comments for delivery, Some comments for delivery',
		deliveryMode: [
			name: 'UPS Ground',
		],
		entries: [
			[
				quantity: 1,
				product: [
					name: 'One In. Rigid Conduit Locknut, 50 Pak',
				],
				totalPrice: [
					formattedValue: '$0.42',
				]
			],
			[
				quantity: 25,
				product: [
					name: '1-1/2 in. Rigid Conduit Locknut',
				],
				totalPrice: [
					formattedValue: '$21.25',
				]
			],
		],
	],
]);
