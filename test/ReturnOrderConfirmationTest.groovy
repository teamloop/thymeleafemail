def ctx = [
	displayName: 'Retailer McRetailer',
	fromEmail: 'CustomerCentral@southwire.com',
	fromDisplayName : 'Southwire Customer Central',
	email: 'retail@garvin.com',
	secureBaseUrl: 'https://southwiredirect.local:9002/direct',
	mediaSecureBaseUrl: 'https://southwiredirect.local:9002/direct',
	customerEmail: 'test@test.com',
	orderNumber: '12345',
]

thymeleafTemplateTester.generate('OrderReturnRequestEmailTemplate', 'southwireDirectContentCatalog', 'Online', ctx);
