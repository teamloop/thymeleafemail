import de.hybris.platform.core.model.c2l.LanguageModel

def ctx = [
	email_language: new LanguageModel('en'), 
	displayName: 'Retailer McRetailer',
	fromEmail: 'CustomerCentral@southwire.com',
	secureResetPasswordUrl: 'http://test.com',
	fromDisplayName : 'Southwire Customer Central',
	email: 'retail@garvin.com',
	secureBaseUrl: 'https://southwiredirect.local:9002/direct',
	mediaSecureBaseUrl: 'https://southwiredirect.local:9002/direct',
]

thymeleafTemplateTester.generate('ForgottenPasswordEmailTemplate', 'southwireDirectContentCatalog', 'Online', ctx);
