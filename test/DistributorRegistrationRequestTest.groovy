import de.hybris.platform.core.model.c2l.LanguageModel

def ctx = [
	email_language: new LanguageModel('en'), 
	displayName: 'Retailer McRetailer',
	fromEmail: 'CustomerCentral@southwire.com',
	fromDisplayName : 'Southwire Customer Central',
	email: 'retail@garvin.com',
	secureBaseUrl: 'https://southwiredirect.local:9002/direct',
	mediaSecureBaseUrl: 'https://southwiredirect.local:9002/direct',
	positionTitle: 'buyer',
	companyName: 'loop integration',
	line1: '10 downing street',
	line2: 'ste 600',
	city: 'aurora',
	distributorState: 'US-CA',
	zip: '80013',
	phone: '5555554444',
	businessPhoneNumber: '5555555555',
	customerData: [
		firstName: 'Retail',
		lastName: 'McRetailer',
		displayUid: 'retail@garvin.com',
	],
]

thymeleafTemplateTester.generate('DistributorRegistrationRequestEmailTemplate', 'southwireDirectContentCatalog', 'Online', ctx);
