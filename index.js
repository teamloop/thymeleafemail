'use strict'

const CSSInliner = require('css-inliner')
const path = require('path')
const fs = require('fs')
const glob = require('glob')
const inliner = new CSSInliner({
	directory: './assets'
})

const dest = path.join(__dirname, 'resources/thymeleafemail/email')

console.log(`Output folder: ${dest}\n`)

if (!fs.existsSync(dest)) {
	fs.mkdirSync(dest);
}

glob(__dirname + '/assets/*.html', {}, (err, files) => {

	if (err) {
		console.error(err)
	}

	files.forEach(file => {

		const base = path.basename(file)
		const html = fs.readFileSync(file, 'utf-8')

		console.log(`Processing: ${base}`)

		inliner
			.inlineCSSAsync(html)
			.then(function (content) {

				const target = path.join(dest, base)

				fs.writeFile(target, content, err => {
					if (err) {
						console.error(err)
					}
				})
			})
	})
})
