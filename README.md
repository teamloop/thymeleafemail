# thymeleafemail

## Description
Use the thymeleaf rendering engine instead of velocity for generating email content.

Email can be created using any html editor. Through the use of Thymeleaf's prototyping,
you can see what the final email will look like immediately.

See the example files within the examples directory.

## Setup
To set the renderer to thymeleaf for an email:

1. Add the thymeleafemail extension to localextensions.xml

2. Add an entry into the spring file

		<bean id="generateForgottenPasswordEmail" parent="abstractThymeleafGenerateEmailAction">
			<property name="frontendTemplateName" value="ForgottenPasswordEmailTemplate" />
		</bean>

3. Set the attributes for the email via impex

		$templatePath = classpath:thymeleafemail/email
		$messagePath = classpath:thymeleafemail/messages

		INSERT_UPDATE RendererTemplate ; messagePath                   ; templatePath             ; rendererType(code)[default = 'thymeleaf']
		                               ; $messagePath/email-properties ; $templatePath/email.html

Where

* messagePath = location of the property file for the email (forgotpass_*.properties)
* templatePath = location of the thymeleaf html template
* rendererType = thymeleaf

Note that either templatePath or templateScript can be used.

* (recommended) templatePath directly renders the template from the filesystem (ie. no need to rerun impex each time the source changes)
* templateScript loads the content into a media object which is loaded during rendering

## Authoring Emails

HTML templates are located in the [thymeleaf ext]/assets

CSS is placed in [thymeleaf ext]/assets/css/inline-style.css

Styles are inlined using npm by either:
* ant all (task in buildcallbacks)
* node index.js (one time)
* npm run watch (monitor the directory for changes)

Styles are inlined and the final version is produced in *[thymeleaf ext]/resources/thymeleafemail/email*

