package com.loopintegration.thymeleafmail.test;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.acceleratorservices.email.CMSEmailPageService;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageTemplateModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.RendererService;
import de.hybris.platform.util.mail.MailUtils;

public class ThymeleafTemplateTester {

	private static final Logger LOG = Logger.getLogger(ThymeleafTemplateTester.class);

	private RendererService rendererService;
	private CMSEmailPageService cmsEmailPageService;
	private CatalogVersionService catalogVersionService;

	public void generate(final String templateName, String catalogId, final String catalogVersionName,
			Map<String, Object> context) {

		final CatalogVersionModel contentCatalogVersion = catalogVersionService.getCatalogVersion(catalogId,
				catalogVersionName);

		final EmailPageModel emailPageModel = cmsEmailPageService.getEmailPageForFrontendTemplate(templateName,
				contentCatalogVersion);
		if (emailPageModel == null) {
			throw new IllegalArgumentException("Invalid template");
		}

		final EmailPageTemplateModel emailPageTemplateModel = (EmailPageTemplateModel) emailPageModel
				.getMasterTemplate();
		final RendererTemplateModel bodyRenderTemplate = emailPageTemplateModel.getHtmlTemplate();
		final RendererTemplateModel subjectRenderTemplate = emailPageTemplateModel.getSubject();

		final StringWriter subject = new StringWriter();
		rendererService.render(subjectRenderTemplate, context, subject);

		final StringWriter body = new StringWriter();
		rendererService.render(bodyRenderTemplate, context, body);

		try {
			final HtmlEmail email = (HtmlEmail) MailUtils.getPreConfiguredEmail();
			email.setCharset("UTF-8");

			final InternetAddress address = new InternetAddress("test@test.com");
			address.setPersonal("email test");

			email.setTo(Collections.singletonList(address));
			email.setFrom("test@test.com", "test");
			email.setSubject(subject.toString());
			email.setHtmlMsg(body.toString());

			email.send();

		} catch (final EmailException | AddressException | UnsupportedEncodingException e) {
			LOG.error(e);
		}
	}

	@Required
	public void setCmsEmailPageService(CMSEmailPageService cmsEmailPageService) {
		this.cmsEmailPageService = cmsEmailPageService;
	}

	@Required
	public void setCatalogVersionService(CatalogVersionService catalogVersionService) {
		this.catalogVersionService = catalogVersionService;
	}

	@Required
	public void setRendererService(final RendererService rendererService) {
		this.rendererService = rendererService;
	}
}
