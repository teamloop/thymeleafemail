package com.loopintegration.thymeleafmail.constants;

@SuppressWarnings({"deprecation","PMD","squid:CallToDeprecatedMethod"})
public class ThymeleafemailConstants extends GeneratedThymeleafemailConstants
{
	public static final String EXTENSIONNAME = "thymeleafemail";
	
	private ThymeleafemailConstants()
	{
		//empty
	}
	
	
}
