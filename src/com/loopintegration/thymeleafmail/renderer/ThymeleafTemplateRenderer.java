package com.loopintegration.thymeleafmail.renderer;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.Resource;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.commons.renderer.Renderer;
import de.hybris.platform.commons.renderer.exceptions.RendererException;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.media.MediaService;

public class ThymeleafTemplateRenderer implements Renderer {

	private static final Logger LOG = Logger.getLogger(ThymeleafTemplateRenderer.class);

	public static final String EMAIL_TEMPLATE_ENCODING = "UTF-8";

	private MediaService mediaService;
	private String contextName;

	public TemplateEngine emailTemplateEngine(RendererTemplateModel template) {

		final SpringTemplateEngine templateEngine = new SpringTemplateEngine();

		templateEngine.addTemplateResolver(stringTemplateResolver());
		templateEngine.setTemplateEngineMessageSource(emailMessageSource(template.getMessagePath()));

		return templateEngine;
	}

	private ITemplateResolver stringTemplateResolver() {

		final StringTemplateResolver templateResolver = new StringTemplateResolver();

		templateResolver.setTemplateMode("HTML");
		templateResolver.setCacheable(false);

		return templateResolver;
	}

	public MessageSource emailMessageSource(String resourcePath) {

		final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();

		messageSource.setBasename(resourcePath);
		messageSource.setCacheSeconds(1);
		messageSource.setResourceLoader(new FileSystemResourceLoader());
		messageSource.setFallbackToSystemLocale(true);
		messageSource.setDefaultEncoding(EMAIL_TEMPLATE_ENCODING);

		return messageSource;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void render(final RendererTemplateModel template, final Object context, final Writer output) {

		final Context ctx;

		if (context instanceof AbstractEmailContext) {
			ctx = createContext((AbstractEmailContext<BusinessProcessModel>) context);
		} else {
			ctx = createContext((Map<String, Object>) context);
		}

		String htmlContent = loadContent(template);

		final String result = emailTemplateEngine(template).process(htmlContent, ctx);

		if (LOG.isDebugEnabled()) {
			LOG.debug("\nCONTENT:\n\n" + result + "\n");
		}

		try {
			output.write(result);
		} catch (IOException e) {
			throw new RendererException("Problem during rendering", e);
		}
	}

	@SuppressWarnings("deprecation")
	private String loadContent(final RendererTemplateModel template) {

		if (template.getTemplatePath() != null) {

			Resource resource = new FileSystemResourceLoader().getResource(template.getTemplatePath());

			try {
				return FileUtils.readFileToString(resource.getFile(), StandardCharsets.UTF_8);

			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}
		}

		final MediaModel content = Optional.ofNullable(template.getContent()).orElse(template.getDefaultContent());
		if (content == null) {
			throw new RendererException("No content found for template " + template.getCode());
		}

		InputStream inputStream = mediaService.getStreamFromMedia(content);
		try {
			return IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());

		} catch (final IOException e) {
			throw new RendererException("Problem during rendering", e);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}

	}

	private Context createContext(Map<String, Object> context) {
		LanguageModel language = (LanguageModel) context.get("email_language");

		Context ctx = new Context(new Locale(language == null ? "en" : language.getIsocode()), context);
		ctx.setVariable(contextName, context);

		return ctx;
	}

	private Context createContext(final AbstractEmailContext<BusinessProcessModel> context) {

		LanguageModel language = (LanguageModel) context.get("email_language");

		final Context ctx = new Context(new Locale(language == null ? "en" : language.getIsocode()));
		StringBuilder sb = new StringBuilder();

		for (Object key : context.internalGetKeys()) {
			if (key instanceof String) {
				ctx.setVariable((String) key, context.get((String) key));

				if (LOG.isDebugEnabled()) {
					sb.append(key + ":" + context.get((String) key) + "\n");
				}
			}
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("\nCONTEXT:\n\n" + sb.toString());
		}

		ctx.setVariable(contextName, context);

		return ctx;
	}

	@Required
	public void setMediaService(final MediaService mediaService) {
		this.mediaService = mediaService;
	}

	@Required
	public void setContextName(final String contextName) {
		this.contextName = contextName;
	}
}
